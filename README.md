# Diagramme de paquetage - Jeu de cartes Loup garou

![Jeu de carte Loup garou](https://media.discordapp.net/attachments/447796473938182155/1114996448262623334/Packtage_diagram.drawio_4.png?width=842&height=676)

Le diagramme de paquetage comprend trois projets dans la solution :

## Projet "Tests"

Ce projet contient les tests unitaires qui se concentrent sur le code de la partie modèle. Les tests dépendent du projet "Model" pour vérifier le comportement attendu des fonctionnalités du modèle.

## Projet "Model"

Ce projet contient les classes, les stubs, la sérialisation et la logique de l'application. Les données utilisées pour afficher les détails d'une carte sont initialisées dans les stubs du modèle. Les autres parties de l'application, y compris le projet "Views", dépendent du modèle pour accéder et afficher les données.

Dans Classes L'`IDataManager` fournit une interface pour accéder et gérer les données nécessaires pour le fonctionnement du modèle, il permet d'instancier un manager en utilisant soit un stub, soit l'xml, c'est l'injection de dépendance.

Le projet "Model" comprend également la sérialisation, permettant de convertir les objets en une représentation sérialisée, en XML, et inversement. Cela facilite le stockage et l'échange de données dans l'application.

## Projet "MauiApp1" (ou "Views")

Ce projet contient les vues de l'application. Les vues utilisent les données du modèle pour afficher les différentes pages, notamment la page de détails de la carte. Les vues dépendent du projet "Model" pour accéder aux données nécessaires à l'affichage.

Les vues sont responsables de la présentation des données au sein de l'interface utilisateur de l'application. Elles utilisent les fonctionnalités fournies par le projet "Model" pour récupérer et afficher les informations pertinentes. Ainsi, les utilisateurs peuvent interagir avec l'application et visualiser les différentes pages et fonctionnalités proposées par le jeu de cartes Loup garou.



# Diagramme de classe

```plantuml
@startuml
Class IdataManager {
	.. Gestion Persistance ..
    + IEnumerable<Carte> loadCarte()
    + void saveCarte(List<Carte>)
    + IEnumerable<Pack> loadPack()
    + void savePack(List<Pack>)
    + IEnumerable<User> loadUser()
    + void saveUser(List<User)
}

Class Manager {
	- ReadOnlyObservableCollection<Carte> Cartes
    - ReadOnlyObservableCollection<Pack> Packs
    - ReadOnlyObservableCollection<User> User
	.. Methodes Cartes ..
	+ Carte addCarte(Carte c)
    + Carte removeCarte(Carte c)
    + List<Carte> getCartes(Carte c)
	.. Methodes Pack ..
    + Pack addPack(Pack p)
    + Pack removePack(Pack p)
    + List<Pack> getPack(Pack p)
	.. Methodes User ..
    + User addUser(User u)
    + User removePack(Pack p)
    + List<User> getUser(User u)
	.. Gestion Persistance ..
    + IEnumerable<Carte> loadCarte()
    + void saveCarte(List<Carte>)
    + IEnumerable<Pack> loadPack()
    + void savePack(List<Pack>)
    + IEnumerable<User> loadUser()
    + void saveUser(List<User)
} 
Class User {
    - String pseudo
    - String mot_de_passe
}

Class Carte {
    + event INotiFyPropertyChanged PropertyChanged
    - String nom
    - String description
    - String pouvoir
    - String strategies
    - int note
    - String lienImage
    - Set<User> likes
    + void addCommentaire(User)
    + void removeCommentaire(User)
    + bool hasLiked(User)
    + void addLike(User)
    + void removeLike(User)
}

Class Commentaire {
    + event INotiFyPropertyChanged PropertyChanged
    - String contenu
    - int signale
    - int nblike
    - int nbdislike
    - Set<User> likes
    + bool hasLiked(User)
    + void addLike(User)
    + void removeLike(User)
}

Class Pack {
    + event INotiFyPropertyChanged PropertyChanged
    - String nom
    - String description
    - int note
    - String lienImage
    - Set<Carte> cartes
    - Set<User> likes
    + void addCommentaire(User)
    + void removeCommentaire(User)
    + bool hasLiked(User)
    + void addLike(User)
    + void removeLike(User)
}

Class Camp {
    - String nom
    - String description
    - String lienImage
    - Set<Carte> cartes
}

User --> Pack
User --> Carte
User --* Commentaire : auteur
Commentaire <-- Pack : List lCommentaires
Commentaire <-- Carte : List lCommentaires
Pack --|> Carte
Camp --|> Carte

Manager --> Carte
Manager --> Pack
Manager --> User
Manager o-- IdataManager : IDataManager (Injection de dépendances)
hide circle
allowmixing
skinparam classAttributeIconSize 0
skinparam classBackgroundColor #ffffb9
skinparam classBorderColor #800000
skinparam classArrowColor #800000
skinparam classFontColor #black
skinparam classFontName Tahoma
class Commentaire #line:green;back:lightblue
@enduml
```

Voici le diagramme de classe général représentant les classes et leurs attributs. Il faut noter que nous avons utiliser les propriété a la place des attributs afin de définir des règles de lecture et d'écriture directement.
__IDataManager__ est une interface qui définit les méthodes pour la gestion de la persistance des données. Elle propose des méthodes pour charger et enregistrer des listes de cartes, de packs et d'utilisateurs.

### Manager
__Manager__ est une classe concrète qui implémente l'interface IDataManager. Elle est responsable de la gestion des collections de cartes, de packs et d'utilisateurs. Elle contient également des méthodes spécifiques pour chaque type d'objet, telles que l'ajout, la suppression et l'obtention de cartes, de packs et d'utilisateurs.

### User
__User__ représente un utilisateur de notre application. Il est capable d'ecrire des commentaires sur les cartes et les packs de jeu.

### Carte
__Carte__ représente une carte du jeu. Elle contient des attributs tels que le nom, la description, le pouvoir, les stratégies, la note, le lien vers l'image, une liste de commentaires et un ensemble d'utilisateurs qui ont aimé cette carte. Elle possède également des méthodes pour gérer les commentaires et les likes.
On utilise INotiFyPropertyChanged pour que la liste des commentaires affichés soit mit a jour apres modification.

### Commentaire
__Commentaire__ représente un commentaire sur une carte ou un pack. Il contient des attributs tels que l'auteur, le contenu, le nombre de signalements, le nombre de likes et de dislikes, ainsi qu'un ensemble d'utilisateurs qui ont aimé ce commentaire. Il possède des méthodes pour gérer les likes.
On utilise INotiFyPropertyChanged pour modifier les commentaires sur la vue si ils sont modifiés.

### Pack
__Pack__ représente un pack de cartes dans le système. Il contient des attributs tels que le nom, la description, la note, le lien vers l'image, un ensemble de cartes, une liste de commentaires et un ensemble d'utilisateurs qui ont aimé ce pack. Il possède également des méthodes pour gérer les commentaires et les likes.
On utilise INotiFyPropertyChanged pour que la liste des commentaires affichés soit mit a jour apres modification.

### Camp
__Camp__ représente un camp de cartes tel que Loup-Garou, Villageois ou encore Solo. Il contient des attributs tels que le nom, la description, le lien vers l'image et un ensemble de cartes. Un camp est une relation d'association entre des cartes.

### Likes
Chaque utilisateur ne peut 'liker' qu'une fois une carte/pack, on utilise donc un set pour avoir des clés uniques, évitant les doublant. Le set nous permet également d'effectuer des opération de recherche plus rapidement pour verifier si un utilisateur à déjà 'liker' ou non.

### Commentaires
Chaque utilisateur peut commenter autant de fois qu'il veut une carte/pack, il peut également supprimer les commentaires qu'il à écrit.

# Diagramme de classe pour la persistance

```plantuml
@startuml
Class stubCarte {
	-List<Carte> lCarte
    - IEnumerable<Carte> CreateCartes()
    + List<Carte> getCartes()
}
Class stubPack{
	-List<Carte> lPack
    - IEnumerable<Pack> CreatePack()}
    + List<Pack> getPack()
}
Class stubCommentaire{
	-List<Commentaire> lCommentaire
    - IEnumerable<Commentaire> CreateCommentaire()
    + List<Commentaire> getCommentaires()
}
Class stubCamp{
	-List<Camp> lCamp
    - IEnumerable<Carte> CreateCartes()
    + List<Carte> getCartes()
}
Class stubUser{
	-List<User> lUser
    - IEnumerable<Carte> CreateUsers()
    + List<Carte> getUsers()
}
Class stubManager {
	.. Gestion Persistance ..
    + IEnumerable<Carte> loadCarte()
    + void saveCarte(List<Carte>)
    + IEnumerable<Pack> loadPack()
    + void savePack(List<Pack>)
    + IEnumerable<User> loadUser()
    + void saveUser(List<User)
	.. Get pour chaque type d'instance ..
    + IEnumerable<Carte> getCartes()
    + IEnumerable<Commentaire> getCommentaires()
    + IEnumerable<Camp> getCamps()
    + IEnumerable<User> getUsers()
    + IEnumerable<Pack> getPack() 
}
Class xmlSerializer{
	.. Gestion Persistance ..
    + IEnumerable<Carte> loadCarte()
    + void saveCarte(List<Carte>)
    + IEnumerable<Pack> loadPack()
    + void savePack(List<Pack>)
    + IEnumerable<User> loadUser()
    + void saveUser(List<User)
}

Class IdataManager {
	.. Gestion Persistance ..
    + IEnumerable<Carte> loadCarte()
    + void saveCarte(List<Carte>)
    + IEnumerable<Pack> loadPack()
    + void savePack(List<Pack>)
    + IEnumerable<User> loadUser()
    + void saveUser(List<User)
}

Class Manager {
	- ReadOnlyObservableCollection<Carte> Cartes
    - ReadOnlyObservableCollection<Pack> Packs
    - ReadOnlyObservableCollection<User> User
	.. Methodes Cartes ..
	+ Carte addCarte(Carte c)
    + Carte removeCarte(Carte c)
    + List<Carte> getCartes(Carte c)
	.. Methodes Pack ..
    + Pack addPack(Pack p)
    + Pack removePack(Pack p)
    + List<Pack> getPack(Pack p)
	.. Methodes User ..
    + User addUser(User u)
    + User removePack(Pack p)
    + List<User> getUser(User u)
	.. Gestion Persistance ..
    + IEnumerable<Carte> loadCarte()
    + void saveCarte(List<Carte>)
    + IEnumerable<Pack> loadPack()
    + void savePack(List<Pack>)
    + IEnumerable<User> loadUser()
    + void saveUser(List<User)
} 


stubManager o-- stubCarte : -stubCarte
stubManager o-- stubPack : -stubPack
stubManager o-- stubUser: -stubUse
stubManager o-- stubCamp : -stubCamp
stubManager o-- stubCommentaire : -stubCommentaire

Manager --> Carte
Manager --> Pack
Manager --> User
Manager o-- IdataManager : iDatamanager (Injection de dépendance)
xmlSerializer ..|> IdataManager
stubManager ..|> IdataManager
hide circle
allowmixing
skinparam classAttributeIconSize 0
skinparam classBackgroundColor #ffffb9
skinparam classBorderColor #800000
skinparam classArrowColor #800000
skinparam classFontColor #black
skinparam classFontName Tahoma
@enduml
```

Les seules données pouvant être modifiées dans le modèle Master-Detail sont les _commentaires_ et les _likes_ des __packs__ et des __cartes__. Ainsi, ce sont les seules données qui sont enregistrées à chaque modification, ajout ou suppression.
Cependant, nous devons charger une premiere fois toutes les données au moins une fois pour avoir notre Mastre-Details.


# Diagramme de séquence 
```plantuml
@startuml
actor User
participant "User Interface" as UI
participant Model
participant Pack
participant Carte

autonumber 
User -> UI: Sélectionne une carte/pack à liker
UI -> Model: Demande de like pour la carte/pack sélectionné(e)
alt Sélection d'un pack
    Model -> Pack: Vérifie si l'utilisateur a déjà liké le pack
    alt Pack déjà liké par l'utilisateur
        Model --> UI: Affiche un message indiquant que le pack a déjà été liké
    else
    autonumber 4
        Model -> Pack: Ajoute le like de l'utilisateur au pack
        Pack --> Model: Confirme l'ajout du like au pack
        Model --> UI: Affiche un message de succès pour le like du pack
    end
else Sélection d'une carte
    autonumber 3
    Model -> Carte: Vérifie si l'utilisateur a déjà liké la carte
    alt Carte déjà likée par l'utilisateur
        Model --> UI: Affiche un message indiquant que la carte a déjà été likée
    else
    autonumber 4
        Model -> Carte: Ajoute le like de l'utilisateur à la carte
        Carte --> Model: Confirme l'ajout du like à la carte
        Model --> UI: Affiche un message de succès pour le like de la carte
    end
end
UI -> User : Affichage d'un coeur indiquant que la carte a déjà été likée
@enduml
```

Lorsque l'utilisateur interagit est sur la page d'une carte ou d'un pack, il a la possibilité d'exprimer son appréciation en likant.

Si l'utilisateur choisit un pack, le modèle commence par vérifier si l'utilisateur l'a déjà liké. Si c'est le cas, le modèle informe l'interface utilisateur que le pack a déjà été liké. Dans le cas contraire, le modèle enregistre le like de l'utilisateur pour le pack sélectionné. Une confirmation est échangée entre le pack et le modèle afin de valider l'ajout du like, suivi de l'envoi d'un message de succès à l'interface utilisateur pour lui indiquer que le like du pack a été enregistré avec succès.

Dans le cas où l'utilisateur sélectionne une carte, le processus est similaire.

Enfin, pour offrir une visualisation de l'état du like, l'interface utilisateur affiche un symbole de cœur pour indiquer que la carte ou le pack a été liké.

# Voici un diagramme représentant l'architecture d'un projet .NET MAUI
![architecture](https://cdn.discordapp.com/attachments/1092005835112198224/1115021122514198588/Diagramme_sans_nom.drawio_1.png)

Le code sera déployé et fonctionnel sur IOS et Android. La documentation du code, quand à elle, se fait en utilisant doxygen.
Le projet en lui-même est dévelopé en utilisant l'outil Drone pour build, tester et publier le projet. Enfin, La sérialisation est réalisée en utilisant XML et enregistrée dans un fichier.xml.
