﻿using Model.Classes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Model.Stub
{
    public class StubCarte
    {
        public ReadOnlyObservableCollection<Carte> LCarte { get; }

        static IEnumerable<Carte> CreateCartes()
        {
            List<Carte> cartes = new()
            {
                new Carte("Villageois", "Le villageois á pour seul pouvoir le vote", "Comme vous n'avez pas de pouvoir, vous devriez vous concentrer sur les autres joueurs pour récupérer des informations", 2, "villageois", "Ses seules armes sont la capacité d’analyse des comportements pour identifier les Loups-Garous, et la force de conviction pour empêcher l’exécution de l’innocent qu’il est."),
                new Carte("Loup-Garou", "Le loup garou se reveille la nuit avec ses confrères pour choisir une personne à dévorer", "Assurez vous de en pas vous faire suspecter par les autres membres du village, pour se faire, évitez de défendre les autres loups garous, ceux qui l'accuse ont surement de bonnes raison de penser qu'il est un Loup. Immiscer un vote sur un Loup Garou peut vous innocenter aux yeux du village.", 4, "loup-garou", "Le loup Garou est un carte interessante à jouer"),
                new Carte("Voyante", "La voyante se reveille la nuit pour regarder le role d'un membre du village", "Trouver les loup garou et conduire le village à voter pour leur expultion et votre objectif, tout en essayant de sauver les innocents, attention, une approche trop directe vous brulerez les ailes, les loup garou essayerons toujours de tuer la voyante dans les premier, soyez subtil", 3, "voyante", "La voyante est un rôle très puissant du village.")
            };
            return cartes;
        }
        public StubCarte()
        {
            LCarte = (ReadOnlyObservableCollection<Carte>)CreateCartes();
        }
    }
}
