﻿using Model.Classes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Model.Stub
{
    public class StubManager : IDataManager
    {
        public StubCarte StubCarte;

        public StubManager()
        {
            StubCarte = new StubCarte();
        }

        public IEnumerable<Carte> GetCartes()
        {
            return StubCarte.LCarte;
        }

        public void SaveCarte(List<Carte> cartes)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<Carte>));

            using (TextWriter writer = new StreamWriter("cartes.xml"))
            {
                serializer.Serialize(writer, cartes);
            }
        }
    }
}
