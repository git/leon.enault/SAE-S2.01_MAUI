﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Classes
{
    public interface IDataManager
    {
        IEnumerable<Carte> GetCartes();
        void SaveCarte(List<Carte> cartes);
    }
}