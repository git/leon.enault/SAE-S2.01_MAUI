﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Data;

namespace Model.Classes
{
    public class Manager : IDataManager
    {
        public ReadOnlyObservableCollection<Carte> Cartes { get; private set; }
        private readonly ObservableCollection<Carte> cartes = new ObservableCollection<Carte>();

        private IDataManager DataMgr { get; set; }
        public Manager(IDataManager dataManager)
        {
            DataMgr = dataManager;
            Cartes = new(cartes);
        }
        public bool AddCarte(Carte carte)
        {
            if (carte == null) throw new ArgumentNullException(nameof(carte));

            if (cartes.Contains(carte)) return false;

            cartes.Add(carte);
            return true;
        }

        public bool RemoveCarte(Carte carte)
        {
            if (carte == null) throw new ArgumentNullException(nameof(carte));

            if (!cartes.Contains(carte)) return false;

            cartes.Remove(carte);
            return true;
        }

        public List<Carte> GetAllCartes()
        {
            return Cartes.ToList();
        }
        public void UpdateCarte(Carte carte)
        {
            if (carte == null)
                throw new ArgumentNullException(nameof(carte));

            int i = Cartes.IndexOf(carte);
            {
                cartes[i] = carte;
            }
        }

        public IEnumerable<Carte> GetCartes()
        {
            throw new NotImplementedException();
        }

        public void SaveCarte(List<Carte> cartes)
        {
            throw new NotImplementedException();
        }
    }
}
