﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Classes
{
    public interface IContenu
    {
        public string Nom { get; set; }
        public string Description { get; set; }
        public int? Note { get; set; }
    }
}
