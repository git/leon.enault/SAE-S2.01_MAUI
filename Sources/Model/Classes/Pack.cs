﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Model.Classes
{
    public class Pack : IContenu, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public string nom;
        public string Nom
        {
            get => nom;
            set
            {
                nom = value;
                OnPropertyChanged(nameof(Nom));
            }
        }
        public string description;
        public string Description
        {
            get => description;
            set
            {
                nom = value;
                OnPropertyChanged(nameof(Description));
            }
        }
        private int? note;
        public int? Note
        {
            get
            {
                return note;
            }
            set
            {
                if (value < 0 || value > 10)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), "La valeur de la note doit être comprise entre 0 et 10.");
                }
                note = value;
                OnPropertyChanged(nameof(Note));
            }
        }
        public List<Commentaire> commentaires;
        public List<Commentaire> Commentaires
        {
            get => commentaires;
            set
            {
                commentaires = value;
                OnPropertyChanged(nameof(Commentaires));
            }
        }
        public Pack(string nom, string description, int? note)
        {
            this.nom = nom;
            this.description = description;
            this.note = note;
            commentaires = new List<Commentaire>();
        }
    }
}
