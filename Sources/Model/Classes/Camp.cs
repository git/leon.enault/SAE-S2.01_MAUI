﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Classes
{
    public class Camp
    {
        public string Nom { get; set; }
        public string Description { get; set; }
        public string LienImage { get; set; }
        public List<Carte> Cartes { get; set; }
        public Camp(string Nom, string Description, string LienImage)
        {
            this.Nom = Nom;
            this.Description = Description;
            this.LienImage = LienImage;
            Cartes = new List<Carte>();
        }
    }
}
