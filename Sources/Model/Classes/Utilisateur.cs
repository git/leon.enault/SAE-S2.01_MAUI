﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Model.Classes
{
    public class Utilisateur : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private readonly string pseudo;
        public string Pseudo => pseudo;
        public string motDePasse;
        public string MotDePasse => motDePasse;
        public string? UrlPdp
        {
            get => urlPdp;
            set
            {
                urlPdp = value;
                OnPropertyChanged(nameof(UrlPdp));
            }
        }
        private string? urlPdp;
        public Utilisateur(string pseudo, string motDePasse, string? urlPdp = null)
        {
            this.pseudo = pseudo;
            this.motDePasse = motDePasse;
            this.urlPdp = urlPdp;
        }
    }
}
