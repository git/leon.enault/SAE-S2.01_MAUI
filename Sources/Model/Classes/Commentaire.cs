﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Classes
{
    public class Commentaire
    {
        public string Texte { get; set; }
        public Utilisateur Proprietaire { get; set; }
        public Commentaire(string Texte, Utilisateur Proprietaire)
        {
            this.Texte = Texte;
            this.Proprietaire = Proprietaire;
        }
    }
}
