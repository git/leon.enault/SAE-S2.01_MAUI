﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Net.Http.Headers;
using System.Runtime.Serialization;

namespace Model.Classes
{
    [DataContract(Name = "carte")]
    public class Carte : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        [DataMember]
        public string Nom {
            get => nom;
            set {
                nom = value;
                OnPropertyChanged(nom);
            } 
        }
        private string nom;
        [DataMember]
        public string Description {
            get => description;
            set
            {
                description = value;
                OnPropertyChanged(nameof(Description));
            }
        }
        private string description;
        [DataMember]
        public string Pouvoir {
            get => pouvoir;
            set
            {
                pouvoir = value;
                OnPropertyChanged(nameof(Pouvoir));
            }
        }
        private string pouvoir;
        [DataMember]
        public string Strategie
        {
            get => strategie;
            set
            {
                strategie = value;
                OnPropertyChanged(nameof(Strategie));
            }
        }
        private string strategie;
        [DataMember]
        public int? Note
        {
            get
            {
                return note;
            }
            set
            {
                if (value < 0 || value > 10)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), "La valeur de la note doit être comprise entre 0 et 10.");
                }
                note = value;
                OnPropertyChanged(nameof(Note));
            }
        }
        private int? note;
        [DataMember]
        public string LienImage
        {
            get => lienImage;
            set
            {
                if (string.IsNullOrEmpty(value)) lienImage = "notfound";
                else lienImage = value;
                OnPropertyChanged(nameof(LienImage));
            }
        }
        public List<Commentaire> commentaires;
        public List<Commentaire> Commentaires
        {
            get => commentaires;
            set
            {
                commentaires = value;
                OnPropertyChanged(nameof(Commentaires));
            }
        }
        private string lienImage;
        public Carte(string nom, string pouvoir, string strategie, int? note, string lienImage, string description)
        {
            this.nom = nom;
            this.description = description;
            this.pouvoir = pouvoir;
            this.strategie = strategie;
            this.note = note;
            this.lienImage = lienImage;
            this.commentaires = new List<Commentaire>();
        }

        public override int GetHashCode()
            => Nom.GetHashCode();

        public override bool Equals(object? right)
        {
            if (right is null) return false;
            if (ReferenceEquals(right, this)) return true;
            if (GetType() != right.GetType()) return false;
            return Equals((Carte)right);
        }


        public bool Equals(Carte other)
        {
            return (this.nom == other.nom);
        }
    }
}
