﻿using Model.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using static System.Console;
using System.Runtime.Serialization.Json;
using System.Xml;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Carte villageois = new Carte("Villageois", "Il peut seulement voter", "Il doit se concentrer sur les autres joueurs", 2, "villageois", "La carte de base");

            Carte loup_garou = new Carte("Loup-Garou", "Il peut se reveiller chaque nuit pour voter avec les autres de son clan qui ils vont manger", "Il doit se faire passer pour un role du village, voter pour un de ses compagnons pour gagner la confiance peut etre un moyen", 4, "loup-garou", "Le loup-garou est un rôle très appréciable à jouer");

            Carte voyante = new("Voyante", "La voyante est capable de voir le role d'un joueur, et ce, chaque nuit", "Elle ne doit pas se faire découvrir, surtout en debut de partie, donner des indices subtiles sans braquer tout les soupçons des loup-garou vers vous est l'objectif", 4, "villageois", "Une carte tres interessante et difficile a la fois, ce role emmene souvent à faire des sacrifices");
            Utilisateur Kylian = new("Kyky", "12345", null);
            Carte C = new("Villageois", "Aucuns", "Doit voter inteligemment", null, "lien", "une carte peu apprécié mais necesaire");
            C.commentaires.Add(new Commentaire("Cette carte est la base du jeu, cool pour les debutants", Kylian ));
            Pack P = new Pack("Jeu de base", "C'est la premiere version du jeu", 5);
            Console.WriteLine(P.Nom);
            Console.WriteLine(C.commentaires[0]);

            string relativePath = "..\\..\\..\\Persistance";

            string xmlFile = Path.Combine(relativePath, "villageois.xml");
            XmlWriterSettings settings = new XmlWriterSettings() { Indent = true };
            var serializer = new DataContractSerializer(typeof(Carte));
            using (TextWriter tw = File.CreateText(xmlFile))
            {
                using (XmlWriter writer = XmlWriter.Create(tw, settings))
                {
                    serializer.WriteObject(writer, villageois);
                }
            }
            Console.ReadKey();
        }
    }
}
