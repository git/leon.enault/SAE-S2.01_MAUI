﻿using Model.Classes;
using System.Diagnostics;

namespace MauiApp1;

public partial class MainPage : ContentPage
{
    public Carte MaCarte { get; private set; }
        = new Carte("Villageois", "Il peut voter", "Soit attentif", null, "image", "Le villageois n'est pas une carte très aprécié");

    public MainPage()
    {
        InitializeComponent();
        BindingContext = MaCarte;
    }

    void Button_Clicked(object sender, EventArgs e)
    {
        Debug.WriteLine($"Just before MaCarte modification: {MaCarte}");
        MaCarte.Note = 5;
        MaCarte.Description = "Une super carte !";
        Debug.WriteLine($"Just after MaCarte modification: {MaCarte}");
    }
}

